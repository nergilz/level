package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"

	"github.com/0LuigiCode0/library/logger"
	"github.com/gorilla/mux"
	"github.com/syndtr/goleveldb/leveldb"
	"github.com/syndtr/goleveldb/leveldb/filter"
	"github.com/syndtr/goleveldb/leveldb/opt"
	"github.com/syndtr/goleveldb/leveldb/util"
)

type RequestModel struct {
	Key   string      `json:"key"`
	Value *ValueModel `json:"value"`
}

type ValueModel struct {
	Index int64  `json:"index"`
	Data  string `json:"data"`
}

type ResponceModel struct {
	Key   string          `json:"key"`
	Value json.RawMessage `json:"value"`
}

type Server struct {
	Addr   string
	Pin    string
	router *mux.Router
	log    *logger.Logger
	DB     *leveldb.DB
}

func main() {
	log := logger.InitLogger("")
	db, err := InitLevelDB()
	if err != nil {
		log.Fatalf("error open db: %v\n", err)
	}
	defer db.Close()
	log.Service("init db")

	server := InitServer(log, db)
	if err := server.Start(); err != nil {
		log.Fatalf("server: %v\n", err)
	}
}

func InitLevelDB() (*leveldb.DB, error) {
	opt := &opt.Options{
		Filter: filter.NewBloomFilter(10),
	}
	db, err := leveldb.OpenFile("levelDatabase", opt)
	if err != nil {
		return db, err
	}
	return db, nil
}

func InitServer(logger *logger.Logger, db *leveldb.DB) *Server {
	server := &Server{
		Addr:   ":7031",
		router: mux.NewRouter(),
		log:    logger,
		DB:     db,
	}
	return server
}

func (s *Server) Start() error {
	s.handlerChain()
	s.log.Service("start server")
	return http.ListenAndServe(s.Addr, s.router)
}

func (s *Server) handlerChain() {
	s.router = mux.NewRouter()
	s.router.HandleFunc("/get", s.hGetValue)
	s.router.HandleFunc("/create", s.hCreateData)
	s.router.HandleFunc("/find", s.hFindByPrefix).Queries("prefix", "{prefix}")
	s.router.HandleFunc("/find-range", s.hFindWithRange).Queries("start", "{start}", "limit", "{limit}")

	s.log.Service("configure route")
}

// handler
func (s *Server) hGetValue(w http.ResponseWriter, r *http.Request) {
	req := new(RequestModel)
	if err := s.GetBody(r, req); err != nil {
		s.log.Errorf("cannot get body: %v", err)
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("cannot get body"))
		return
	}
	getval, err := s.DB.Get([]byte(req.Key), nil)
	if err != nil {
		s.log.Warningf("cannot get data: %v", err)
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("cannot get data"))
		return
	}
	w.Header().Set("Content-Type", "applicaiton/json")
	w.Write(getval)
	s.log.Info("get data")
	// s.RespOK(w, getval)
}

func (s *Server) hCreateData(w http.ResponseWriter, r *http.Request) {
	req := new(RequestModel)
	if err := s.GetBody(r, req); err != nil {
		s.log.Errorf("cannot get body: %v", err)
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("cannot get body"))
		return
	}
	val, err := json.Marshal(req.Value)
	if err != nil {
		s.log.Errorf("cannot marshal json: %v", err)
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("cannot marshal json"))
		return
	}
	if err := s.DB.Put([]byte(req.Key), json.RawMessage(val), nil); err != nil {
		s.log.Errorf("cannot put: %v", err)
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("cannot put"))
		return
	}
	w.Header().Set("Content-Type", "applicaiton/json")
	w.Write(val)
	s.log.Info("create data")
	// s.RespOK(w, "create data")
}

func (s *Server) hFindByPrefix(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	prefix := []byte(vars["prefix"])
	data, err := s.SearchByPrefix(prefix)
	if err != nil {
		s.log.Warningf("cannot search by prefix: %v", err)
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("cannot search by prefix"))
		return
	}
	resp, err := json.Marshal(data)
	if err != nil {
		s.log.Warningf("cannot decode resp to json: %v", err)
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("cannot decode resp to json"))
		return
	}
	w.Header().Set("Content-Type", "applicaiton/json")
	w.Write(resp)
	s.log.Info("find by prefix")
	// s.RespOK(w, resp)
}

func (s *Server) hFindWithRange(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	start := []byte(vars["start"])
	limit := []byte(vars["limit"])
	data, err := s.SearchWithRange(start, limit)
	if err != nil {
		s.log.Warningf("cannot search data in db: %v", err)
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("cannot search data in db"))
		return
	}
	resp, err := json.Marshal(data)
	if err != nil {
		s.log.Warningf("cannot decode prefix json: %v", err)
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("cannot decode prefix"))
		return
	}
	w.Header().Set("Content-Type", "applicaiton/json")
	w.Write(resp)
	s.log.Info("find with range")
	// s.RespOK(w, resp)
}

// store
func (s *Server) SearchByPrefix(prefix []byte) ([]*ResponceModel, error) {
	res := make([]*ResponceModel, 0)

	iter := s.DB.NewIterator(util.BytesPrefix(prefix), nil)
	for iter.Next() {
		byPrefix := new(ResponceModel)
		byPrefix.Key = string(iter.Key())
		byPrefix.Value = iter.Value()
		res = append(res, byPrefix)
	}
	iter.Release()
	if err := iter.Error(); err != nil {
		return nil, err
	}
	return res, nil
}

func (s *Server) SearchWithRange(start, limit []byte) ([]*ResponceModel, error) {
	result := make([]*ResponceModel, 0)

	iter := s.DB.NewIterator(&util.Range{Start: []byte(start), Limit: []byte(limit)}, nil)
	for iter.Next() {
		byRange := new(ResponceModel)
		byRange.Key = string(iter.Key())
		byRange.Value = iter.Value()
		result = append(result, byRange)
	}
	iter.Release()
	if err := iter.Error(); err != nil {
		return nil, err
	}
	return result, nil
}

// helper
func (s *Server) RespOK(w http.ResponseWriter, dataStruct interface{}) {
	w.Header().Set("Content-Type", "applicaiton/json")
	buf, err := json.Marshal(dataStruct)
	if err != nil {
		s.log.Warningf("cannot marshal json: %v", err)
		w.Write([]byte("cannot marshal json"))
		return
	}
	_, err = w.Write(buf)
	if err != nil {
		s.log.Warningf("cannot write responce: %v", err)
		w.Write([]byte("cannot write responce"))
		return
	}
}

func (s *Server) GetBody(r *http.Request, data interface{}) error {
	buf, err := io.ReadAll(r.Body)
	if err != nil {
		return fmt.Errorf("read body is invalid: %v", err)
	}
	err = json.Unmarshal(buf, data)
	if err != nil {
		return fmt.Errorf("unmarshal is invalid: %v", err)
	}
	r.Body = io.NopCloser(bytes.NewReader(buf))
	return nil
}
